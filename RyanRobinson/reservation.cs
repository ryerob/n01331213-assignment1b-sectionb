﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RyanRobinson
{
    public class Reservation
    {
        public Restaurant restaurant;
        public Patron patron;
        public Meal meal;
        private Meal newmeal;
        private Patron newpatron;
        private object newrestraurant;

        public Reservation(Restaurant r, Patron p, Meal m)
        {
            restaurant = r;
            patron = p;
            meal = m;
        }

        public Reservation(Meal newmeal, Patron newpatron, object newrestraurant)
        {
            this.newmeal = newmeal;
            this.newpatron = newpatron;
            this.newrestraurant = newrestraurant;
        }

        public string PrintConfirmation()
        {
            string reservationCofirmation = "Your reservation is confirmed as: <br />";
            reservationCofirmation += "Name: " + patron.PatronName + "<br />";
            reservationCofirmation += "Phone Number: " + patron.PatronPhone + "<br />";
            reservationCofirmation += "E-mail: " + patron.PatronEmail + "<br />";

            reservationCofirmation += "Party Size: " + restaurant.partySize + "<br />";
            reservationCofirmation += "Reservation Time: " + restaurant.reservationTime + "<br />";
            reservationCofirmation += "Table Location: " + restaurant.tableLocation + "<br />";

            return reservationCofirmation;
        }

      
    }
}