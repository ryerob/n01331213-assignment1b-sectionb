﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace RyanRobinson
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        protected void Order(object sender, EventArgs e)
        {
            if (!Page.IsValid)
            {
                return;
            }
            thankYouMessage.InnerHtml = "Thank you for making a reservation.";


            int partysize = int.Parse(reservationPartySize.Text);
            Restaurant newrestaurant = new Restaurant(partysize);

            string name = patronName.Text.ToString();
            string phone = patronPhone.Text.ToString();
            string email = patronName.Text.ToString();
            Patron newpatron = new Patron();
            newpatron.PatronName = name;
            newpatron.PatronName = phone;
            newpatron.PatronName = email;


            string currentDate = DateTime.Today.ToString("dd-mm-yyyy");
            string timeReserved = reservationTime.Text.ToString();
            string hour = timeReserved.Substring(0, 2);
            string minuet = timeReserved.Substring(timeReserved.Length - 2);
            string timeMessage = currentDate + " " + hour + ":" + minuet;
            // DateTime reservationtTime = DateTime.ParseExact(timeMessage, "dd-MM-yyyy HH:mm", CultureInfo.InvariantCulture); 

            
            List<String> allergylist = new List<String> { };
            List<String> additionalmenus = new List<String> { };
            Meal newmeal = new Meal(allergylist, additionalmenus);
            List<string> allergies = new List<string>();
            foreach (Control control in allergyList.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox allergy = (CheckBox)control;
                    if (allergy.Checked)
                    {
                        allergies.Add(allergy.Text);
                    }
                }
            }
            newmeal.allergies = newmeal.allergies.Concat(allergies).ToList();

            List<string> menus = new List<string>();

            foreach (Control control in additionalMenus.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox menu = (CheckBox)control;
                    if (menu.Checked)
                    {
                        menus.Add(menu.Text);
                    }
                }
            }
            newmeal.additionalMenus = newmeal.additionalMenus .Concat(menus).ToList();

            Reservation newreservation = new Reservation(newrestaurant, newpatron, newmeal);

            confirmationMessage.InnerHtml = newreservation.PrintConfirmation();



        }
    }
}