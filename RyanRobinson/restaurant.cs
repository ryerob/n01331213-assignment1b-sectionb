﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
// The restaurant class includes all information regaurding the physical space to be able to determin capacity and availability of future reservations
namespace RyanRobinson
{
    public class Restaurant
    {
        public int partySize;
        public DateTime reservationTime;
        public string tableLocation;

        public Restaurant(int partysize)
        {
            partySize = partysize;
        }

        public Restaurant(int ps, DateTime rt, string tl)
        {
            partySize = ps;
            reservationTime = rt;
        }
    }
}