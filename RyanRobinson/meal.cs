﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//The meal class holds all information pertaining to the ordering of food that relate to the wait staff and kitchen once the diner is seated
namespace RyanRobinson
{
    public class Meal
    {
        public List<string> allergies;
        public List<string> additionalMenus;

        public Meal(List<string> al, List<string> am)
        {
            allergies = al;
            additionalMenus = am;
        }

    }
}