﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="servercontrols.aspx.cs" Inherits="RyanRobinson.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Dinner Reservation</title>
</head>
<body>
    <form id="form1" runat="server">
        <h1>Dinner Reservation</h1>
        <div>
            <asp:Label runat="server">Name: </asp:Label> 
            <asp:TextBox runat="server" ID="patronName" placeholder="e.g. John Doe"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your name." ControlToValidate="patronName" ID="validatorName"></asp:RequiredFieldValidator>
            </div>
            <div>
            <asp:Label runat="server">Phone: </asp:Label>
            <asp:TextBox runat="server" ID="patronPhone" placeholder="e.g. 9052335555"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your phone number." ControlToValidate="patronPhone" ID="validatorPhone"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="patronPhone" Type="String" Operator="NotEqual" ValueToCompare="9052337234" ErrorMessage="This is our phone number."></asp:CompareValidator>
            
            <%-- ValidationExpression used for phone number found at https://stackoverflow.com/questions/18585613/how-do-you-validate-phone-number-in-asp-net--%> 
           
            <asp:RegularExpressionValidator ID="phoneValid" runat="server" ErrorMessage="Please enter a valid phone number."  ControlToValidate="patronPhone" ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$" ></asp:RegularExpressionValidator>
        </div>
        <div>
            <asp:Label runat="server">E-mail: </asp:Label>
            <asp:TextBox runat="server" ID="patronEmail" placeholder="e.g. johndoe@johnweb.net"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter your e-mail address." ControlToValidate="patronEmail" ID="validatorEmail"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="emailValid" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="patronEmail" ErrorMessage="Invalid Email Format"></asp:RegularExpressionValidator>
        </div>
        <hr />
        <div>
            <asp:Label runat="server">Party Size: </asp:Label>
            <asp:DropDownList runat="server" ID="reservationPartySize">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                <asp:ListItem Value="11" Text="11"></asp:ListItem>
                <asp:ListItem Value="12" Text="12"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div>
            <asp:Label runat="server">Reservation Time (P.M.): </asp:Label>
            <asp:DropDownList runat="server" ID="reservationTime">
                <asp:ListItem Value="1700" Text="5:00"></asp:ListItem>
                <asp:ListItem Value="1730" Text="5:30"></asp:ListItem>
                <asp:ListItem Value="1800" Text="6:00"></asp:ListItem>
                <asp:ListItem Value="1830" Text="6:30"></asp:ListItem>
                <asp:ListItem Value="1900" Text="7:00"></asp:ListItem>
                <asp:ListItem Value="1930" Text="7:30"></asp:ListItem>
                <asp:ListItem Value="2000" Text="8:00"></asp:ListItem>
                <asp:ListItem Value="2030" Text="8:30"></asp:ListItem>
                <asp:ListItem Value="2100" Text="9:00"></asp:ListItem>
                <asp:ListItem Value="2130" Text="9:30"></asp:ListItem>
                <asp:ListItem Value="2200" Text="10:00"></asp:ListItem>
                <asp:ListItem Value="2230" Text="10:30"></asp:ListItem>
            </asp:DropDownList>
        </div>

        <%-- Method of default radio button list selection from https://stackoverflow.com/questions/24978384/checkboxlist-items-as-checked-by-default-in-codebehind-asp-net --%>

        <div>
            <asp:Label runat="server">Table Location: </asp:Label>
            <asp:RadioButtonList runat="server" ID="tableLocation">
                <asp:ListItem Text="Dinning Room" Selected="True" >Dinning Room</asp:ListItem>
                <asp:ListItem Text="Patio">Patio</asp:ListItem>
            </asp:RadioButtonList>
        </div>
        <hr />
        <asp:Label runat="server">Additional Menus: </asp:Label>
        <div id="additionalMenus" runat="server">
            <asp:CheckBox runat="server" ID="wineMenu" Text="Wine" />
            <asp:CheckBox runat="server" ID="whiskeyMenu" Text="Whiskey" />
            <asp:CheckBox runat="server" ID="dessertMenu" Text="Dessert" />
        </div>
        <asp:Label runat="server">Alergies: </asp:Label>
        <div id="allergyList" runat="server">
            <asp:CheckBox runat="server" ID="nutAllergy" Text="Nuts" />
            <asp:CheckBox runat="server" ID="dairyAllergy" Text="Dairy" />
            <asp:CheckBox runat="server" ID="glutenAllergy" Text="Gluten" />
            <asp:CheckBox runat="server" ID="otherAllergy" Text="Other" />
        </div>
        <div>
            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ForeColor="Red" DisplayMode="list"/>
        </div>
        <br />
        <div>
            <asp:Button runat="server" ID="submitButton" OnClick="Order" Text="Submit"/>
        </div>
        <div id="thankYouMessage" runat="server">

        </div>
        <div id="confirmationMessage" runat="server">

        </div>
    </form>
</body>
</html>